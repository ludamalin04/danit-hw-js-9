'use strict'

function listMaker(arr, element) {
    const list = document.createElement('ol');
    element.prepend(list);
    for (let key of arr) {
        let listItem = document.createElement('li');
        list.append(listItem);
        listItem.innerText = key;
    }
}

listMaker(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], document.body);
