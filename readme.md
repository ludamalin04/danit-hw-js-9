**1. Створення нового тегу** відбувається через метод createElement або insertAdjacentHTML.

**2. Перший параметр** функції insertAdjacentHTML це місце вставки коду в HTML документ. Є 4 варіанти цього параметра: afterbegin, afterend, beforebegin і beforeend.

**3. Видалити елемент** можна за допомогою функції remove.